#!/usr/bin/env python3

import csv
import sys
import statistics

bpm = float(sys.argv[1])
# midi beat clock at 24 ppqn
# at 120bpm, quarter notes are at 500ms intervals
# -> 500/24 ms intervals
# 60000/(120*24)
quarterNoteInterval = 60000/bpm
timingPulseInterval = quarterNoteInterval/24

reader = csv.reader(sys.stdin)
# skip header
next(reader)


def printStats(arrIn, prefix):
	arr = arrIn
	arr.sort()
	mode = "None"
	try:
		mode = '%.6g' % (statistics.mode(arr))
	except statistics.StatisticsError:
		print("no mode")
	median = statistics.median(arr)
	mean = statistics.mean(arr)
	median_grouped = statistics.median_grouped(arr, interval=0.00001)
	print ("%s: Shortest %.6g   Longest %.6g   Median: %.6g   Mean: %.6g   Mode: %s   Grouped Median: %.6g" % (prefix, arr[0], arr[-1], median, mean, mode, median_grouped))


class Deltas:

	def __init__(self, expectedInterval):
		self.expectedInterval = expectedInterval
		self.deltas = []
		self.lastTime = None
		self.firstTime = None
		self.rollingTimes = []
		self.rollingErrorDeltas = []
		self.rollingTime = 0
		self.expectedRollingTime = 0
		self.rollingError = None

	def addEntry(self, time, verbose=False):
		if self.lastTime == None:
			self.firstTime = time
			self.lastTime = time
			return

		delta = time - self.lastTime
		if verbose:
			print('adding delta ' + str(delta))
		self.lastTime = time

		self.rollingTime = time - self.firstTime
		self.rollingTimes.append(self.rollingTime)
		self.expectedRollingTime = self.expectedInterval * (len(self.deltas) + 1)
		self.deltas.append(delta)

		thisRollingError = self.rollingTime - self.expectedRollingTime
		if self.rollingError != None:
			rollingErrorDelta = thisRollingError - self.rollingError
			self.rollingErrorDeltas.append(rollingErrorDelta)
		self.rollingError = thisRollingError
		#print ("Delta %f: %s" % (delta, message))
		#print("rollingError: %g" % (rollingError))

	def printStats(self, prefix):
		print("%s: At %f bpm, interval should be %.8gms" % (prefix, bpm, self.expectedInterval))
		printStats(self.deltas, prefix + " interval")
		meanInterval = statistics.mean(self.deltas)
		medianError = self.expectedInterval-statistics.median(self.deltas)
		meanError = self.expectedInterval-meanInterval
		print ("%s: median error: %.6g, mean error: %.6g" % (prefix, medianError, meanError))
		printStats(self.rollingErrorDeltas, prefix + " slippage")

		lastEventTime = self.rollingTime
		expectedLastEventTime = self.expectedRollingTime
		print("%s: last event at %.6g (expected %.6g -> slippage %.6gms)" % (prefix, lastEventTime, expectedLastEventTime, lastEventTime-expectedLastEventTime))

		slippage = (self.rollingTime - self.expectedRollingTime)
		slippageRatio = self.rollingTime/self.expectedRollingTime
		slippagePerInterval = (slippageRatio-1.0) * self.expectedInterval
		intervalsPerSecond = 1000/self.expectedInterval
		#slippagePerSecond = slippage / self.expectedRollingTime 
		slippagePerSecond = slippagePerInterval * intervalsPerSecond

		print("%s: slippage %.6gms, ratio %.6g, per interval %.6gms, per second %.6gms, per minute %.6gms" % (prefix, slippage, slippageRatio, slippagePerInterval, slippagePerSecond, slippagePerSecond * 60))
		print("%s: suggested BPM=%.10g" % (prefix, bpm/slippageRatio))



timingClockDeltas = Deltas(timingPulseInterval)
beatDeltas = Deltas(quarterNoteInterval)


for row in reader:
	time = float(row[0]) * 1000
	message = row[1]
	if message == "TimingClock":
		timingClockDeltas.addEntry(time)
	if message == "NoteOn Channel: 9":
		beatDeltas.addEntry(time, verbose=False)

timingClockDeltas.printStats("Timing pulse")
beatDeltas.printStats("Beat")




'''
print("At %f bpm, timing pulse interval should be %.8g" % (bpm, timingPulseInterval))
printStats(deltas, "Timing pulse interval")
print ("Timing pulse interval median error: %.6g, mean error: %.6g" % (abs(timingPulseInterval-statistics.mean(deltas)), abs(timingPulseInterval-statistics.median(deltas))))
printStats(rollingErrorDeltas, "Slippage per pulse")
'''
'''
slippagePerPulse = statistics.mean(rollingErrorDeltas)
quarterNotesPerSecond = bpm/60
slippagePerSecond = slippagePerPulse * quarterNotesPerSecond * 24

print("Mean slippage per second: %g, per minute: %g" %(slippagePerSecond, slippagePerSecond * 60))
'''

'''
after 10 minutes at 128 BPM, we're 196ms out of sync
-> each beat, we gain 0.153125ms

'''


/****************************************************************************
midiDump.cpp
by Damian Stewart 2019

Does not record to file - just dumps to console in a CSV format

 --------------------------
Based on midiRecord.cpp by Nick Knouf 2006
<nknouf@zeitkunst.org>

Simply records MIDI data to a file while listening to a specific MIDI port.
There didn't seem to be any code to do this already, thus I had to write
my own.

Much help came from:
http://www.sonicspot.com/guide/midifiles.html
http://www.borg.com/~jglatt/tech/midifile.htm

The software uses the RtMIDI library by Gary Scavone:
http://www.music.mcgill.ca/~gary/rtmidi/

midiRecord falls under the same license as his library:

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

Any person wishing to distribute modifications to the Software is requested to send the modifications to the original developer so that they can be incorporated into the canonical version.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
****************************************************************************/

#include <iostream>
#include <fstream>
#include <signal.h>
#include <getopt.h>
#include "RtMidi.h"

// Platform-dependent sleep routines.
#if defined(__WINDOWS_MM__)
    #include <windows.h>
    #define SLEEP( milliseconds ) Sleep( (DWORD) milliseconds ) 
#else // Unix variants
    #include <unistd.h>
    #define SLEEP( milliseconds ) usleep( (unsigned long) (milliseconds * 1000.0) )
#endif

bool done;
static void finish(int ignore){ done = true; }

void usage(void) {
    // Error function in case of incorrect command-line
    // argument specifications.
    std::cout << "\n";
    std::cout << "midiRecord by Nick Knouf (nknouf@zeitkunst.org)\n";
    std::cout << "Available from http://zeitkunst.org/projects/midiRecord\n";
    std::cout << "Using the RtMidi library by Gary Scavone.\n";
    std::cout << "http://www.music.mcgill.ca/~gary/rtmidi/\n";
    std::cout << "\n";
    std::cout << "usage: midiRecord <args>\n";
    std::cout << "\t--help, -h\tthis help information\n";
    std::cout << "\t--list, -l\tlist MIDI ports and quit\n";
    std::cout << "\t--port, -p\tMIDI port number to use (default: 0)\n";
    std::cout << "\t--quit, -q\tAutoamtically quit after this many seconds have elapsed (default: never)\n";
    std::cout << "\t--verbose, -v\tDisplay information about bytes being received\n";
    std::cout << "\n";
    exit(0);
}

void listMidiDevices() {
    RtMidiIn *midiin = 0;
    // RtMidiIn constructor
    try {
        midiin = new RtMidiIn();
    }
    catch (RtError &error) {
        error.printMessage();
        exit(EXIT_FAILURE);
    }

    unsigned int nPorts = midiin->getPortCount();
    for (int i=0; i<nPorts; i++) {
        std::cout << "MIDI Port " << i << ": " << midiin->getPortName(i) << std::endl;
    }

    delete midiin;

}

std::string describeStatusByte(unsigned char b)
{
    auto status = b & 0xf0;
    auto channel = b & 0x0f;

    char buf[512];
    switch(status) {
        case 0xf0:
            switch (b) {
                case 0xf1:
                    return "QuarterFrame";
                case 0xf2:
                    return "SongPositionPointer";
                case 0xf8:
                    return "TimingClock";
                case 0xfa:
                    return "Start";
                case 0xfb:
                    return "Continue";
                case 0xfc:
                    return "Stop";
                case 0xff:
                    return "Meta";
            }
        case 0x80:
            sprintf(buf, "NoteOff Channel: %i", channel);
            return buf;
        case 0x90:
            sprintf(buf, "NoteOn Channel: %i", channel);
            return buf;
        case 0xb0:
            sprintf(buf, "CC Channel: %i", channel);
            return buf;

    }

    // fallback
    return "Unknown";
}

std::string describeTime(double f) {
	char buf[16];
	sprintf(buf, "%f", f);
	return buf;
}

std::string describeByte(unsigned char b)
{
    char buf[16];
    sprintf(buf, "0x%x(%i)", b, b);
    return buf;
}

// Here we go!
int main(int argc, char *argv[])
{
    RtMidiIn *midiin = 0;
    std::vector<unsigned char> message;
    int nBytes = 0, i;
    int quitTimeSeconds = -1;
    double stamp;
    double runningStamp = 0;
    unsigned long deltaTime = 0;
    unsigned long numBytes = 0;
    FILE *midiOutFile;

    // things that could get set from the command-line
    unsigned int port = 0;
    static int verbose;
   
    // needed for getopt
    int c;

    // parse command-line options using getopt_long
    // Mostly from: http://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Option-Example.html#Getopt-Long-Option-Example
    while (1) {
        static struct option longOptions[] = {
            // define how loud we are
            {"verbose", no_argument, &verbose, 1},
            {"brief", no_argument, &verbose, 0},
            // options requiring arguments
            {"quit", required_argument, 0, 'q'},
            {"file", required_argument, 0, 'f'},
            {"port", required_argument, 0, 'p'},
            {"help", no_argument, 0, 'h'},
            {"list", no_argument, 0, 'l'},
            {0, 0, 0, 0}
        };

        int optionIndex = 0;

        c = getopt_long(argc, argv, "hlf:p:q:", longOptions, &optionIndex);

        if (c == -1) break;

        switch (c) {
            case 0:
                if (longOptions[optionIndex].flag != 0) break;
                break;

            case 'p':
                port = (unsigned int) atoi(optarg);
                break;

            case 'q':
                quitTimeSeconds = atoi(optarg);
                break;

            case 'l':
                listMidiDevices();
                exit(0);

            case 'h':
                usage();
                exit(0);

            case '?':
                usage();
                exit(1);

            default:
                usage();
                exit(1);
        }
    }

    // RtMidiIn constructor
    try {
        midiin = new RtMidiIn();
    }
    catch (RtError &error) {
        error.printMessage();
        exit(EXIT_FAILURE);
    }

    // Check available ports vs. specified.
    unsigned int nPorts = midiin->getPortCount();
    if ( port >= nPorts ) {
        delete midiin;
        std::cout << "Invalid port specifier!\n";
        usage();
    }

    try {
        midiin->openPort( port );
    }
    catch (RtError &error) {
        error.printMessage();
        goto cleanup;
    }

    // don't ignore sysex, timing, or active sensing messages.
    midiin->ignoreTypes( false, false, false );

    // Install an interrupt handler function.
    done = false;
    (void) signal(SIGINT, finish);

    // Periodically check input queue.
    //std::cout << "Reading MIDI from port ... quit with Ctrl-C.\n";

    while ( (!done) || (nBytes != 0) ) {
        stamp = midiin->getMessage( &message );
        runningStamp += stamp;
        nBytes = message.size();

        if ( nBytes > 0 ) {
            if (verbose) {std::cout << "stamp = " << stamp << " (running " << runningStamp << "): ";}
            deltaTime = (unsigned long) (1000 * stamp);
            if (verbose) {std::cout << "stamp (ms) = " << deltaTime << " ";}
            std::cout << describeTime(runningStamp) << ",";
            std::cout << describeStatusByte(message[0]) << ",";

            for ( i=0; i<nBytes; i++ ) {
                std::cout << describeByte(message[i]) << ",";
            }
            std::cout << std::endl;
            std::flush(std::cout);

        }

        if (quitTimeSeconds > 0 && runningStamp > quitTimeSeconds) {
            break;
        }

        // Sleep for 10 milliseconds.
        SLEEP( 10 );
    }

    // Clean up
 cleanup:
    midiin->closePort();
    delete midiin;

    return 0;
}

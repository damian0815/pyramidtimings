/****************************************************************************
midiRecord.cpp
by Nick Knouf 2006
<nknouf@zeitkunst.org>

Simply records MIDI data to a file while listening to a specific MIDI port.
There didn't seem to be any code to do this already, thus I had to write
my own.

Much help came from:
http://www.sonicspot.com/guide/midifiles.html
http://www.borg.com/~jglatt/tech/midifile.htm

The software uses the RtMIDI library by Gary Scavone:
http://www.music.mcgill.ca/~gary/rtmidi/

midiRecord falls under the same license as his library:

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

Any person wishing to distribute modifications to the Software is requested to send the modifications to the original developer so that they can be incorporated into the canonical version.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
****************************************************************************/

#include <iostream>
#include <fstream>
#include <signal.h>
#include <getopt.h>
#include "RtMidi.h"

// Platform-dependent sleep routines.
#if defined(__WINDOWS_MM__)
    #include <windows.h>
    #define SLEEP( milliseconds ) Sleep( (DWORD) milliseconds ) 
#else // Unix variants
    #include <unistd.h>
    #define SLEEP( milliseconds ) usleep( (unsigned long) (milliseconds * 1000.0) )
#endif

bool done;
static void finish(int ignore){ done = true; }

// Standard MIDI Header
// Change the final pair of bytes to modify the time division
unsigned char midiHeaderHex[] = {0x4D, 0x54, 0x68, 0x64, // MThd
                                0x00, 0x00, 0x00, 0x06, // six byte chunk
                                0x00, 0x00, // type 0, for now
                                0x00, 0x01, // one track
                                //0x03, 0xe8 // 1000 ticks per beat
                                //0x00, 0x78 // 120 ticks per beat
                                0x99, 0x28 // (SMPTE) 25 fps, 40 ticks per frame
                                };

// Chunk header
unsigned char midiChunkHex[] = {0x4D, 0x54, 0x72, 0x6B, // MTrk
                                0x00, 0x00, 0x00, 0x00 // chunk size, filled in
                                                        // later
                                };

void usage(void) {
    // Error function in case of incorrect command-line
    // argument specifications.
    std::cout << "\n";
    std::cout << "midiRecord by Nick Knouf (nknouf@zeitkunst.org)\n";
    std::cout << "Available from http://zeitkunst.org/projects/midiRecord\n";
    std::cout << "Using the RtMidi library by Gary Scavone.\n";
    std::cout << "http://www.music.mcgill.ca/~gary/rtmidi/\n";
    std::cout << "\n";
    std::cout << "usage: midiRecord <args>\n";
    std::cout << "\t--file, -f\toutput filename (default: output.mid)\n";
    std::cout << "\t--help, -h\tthis help information\n";
    std::cout << "\t--port, -p\tMIDI port number to use (default: 0)\n";
    std::cout << "\t--verbose, -v\tDisplay information about bytes being received\n";
    std::cout << "\n";
    exit(0);
}

// From http://www.borg.com/~jglatt/tech/midifile.htm
int writeVarLen(register unsigned long value, FILE* outputFile) {
    register unsigned long buffer;
    buffer = value & 0x7F;

    int numBytesWritten = 0;

    while ((value >>= 7)) {
        buffer <<= 8;
        buffer |= ((value & 0x7F) | 0x80);
    }

    while (1) {
        putc(buffer, outputFile);
        numBytesWritten += 1;
        if (buffer & 0x80) {
            buffer >>= 8;
        } else {
            break;
        }
    }

    return numBytesWritten;
}

// Seems to cause an extra ~3 seconds in the output file...
int writeMidiText(int eventType, char* text, FILE* outputFile) {
    int numBytes = 0;

    putc(0x00, outputFile); numBytes +=1; // delta 0
    putc(0xFF, outputFile); numBytes +=1; // meta event
    fwrite(&eventType, sizeof(int), 1, outputFile); numBytes +=1;
    //putc(0x04, outputFile); numBytes +=1; // type: instrument name
    numBytes += writeVarLen(strlen(text), outputFile); // length of name
    fwrite(text, sizeof(char), strlen(text), outputFile);
                                            // name itself
    numBytes += strlen(text);

    return numBytes;
}

void listMidiDevices() {
    RtMidiIn *midiin = 0;
    // RtMidiIn constructor
    try {
        midiin = new RtMidiIn();
    }
    catch (RtError &error) {
        error.printMessage();
        exit(EXIT_FAILURE);
    }

    unsigned int nPorts = midiin->getPortCount();
    for (int i=0; i<nPorts; i++) {
        std::cout << "MIDI Port " << i << ": " << midiin->getPortName(i) << std::endl;
    }

    delete midiin;

}

// Here we go!
int main(int argc, char *argv[])
{
    RtMidiIn *midiin = 0;
    std::vector<unsigned char> message;
    int nBytes = 0, i;
    int quitTimeSeconds = -1;
    double stamp;
    double runningStamp = 0;
    unsigned long deltaTime = 0;
    unsigned long numBytes = 0;
    FILE *midiOutFile;

    // things that could get set from the command-line
    unsigned int port = 0;
    char* outputFilename = "output.mid";
    static int verbose;
   
    // needed for getopt
    int c;

    // parse command-line options using getopt_long
    // Mostly from: http://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Option-Example.html#Getopt-Long-Option-Example
    while (1) {
        static struct option longOptions[] = {
            // define how loud we are
            {"verbose", no_argument, &verbose, 1},
            {"brief", no_argument, &verbose, 0},
            // options requiring arguments
            {"quit", required_argument, 0, 'q'},
            {"file", required_argument, 0, 'f'},
            {"port", required_argument, 0, 'p'},
            {"help", no_argument, 0, 'h'},
            {"list", no_argument, 0, 'l'},
            {0, 0, 0, 0}
        };

        int optionIndex = 0;

        c = getopt_long(argc, argv, "hlf:p:q:", longOptions, &optionIndex);

        if (c == -1) break;

        switch (c) {
            case 0:
                if (longOptions[optionIndex].flag != 0) break;
                break;

            case 'f':
                outputFilename = optarg;
                break;

            case 'p':
                port = (unsigned int) atoi(optarg);
                break;

            case 'q':
                quitTimeSeconds = atoi(optarg);
                break;

            case 'l':
                listMidiDevices();
                exit(0);

            case 'h':
                usage();
                exit(0);

            case '?':
                usage();
                exit(1);

            default:
                usage();
                exit(1);
        }
    }

    midiOutFile = fopen(outputFilename, "wb");
    fwrite(midiHeaderHex, sizeof(char), 14, midiOutFile);
    fwrite(midiChunkHex, sizeof(char), 8, midiOutFile);

    // write some meta information, since we've got only one track
    // need to keep track of bytes written
    const char *instrumentName = "Recorded MIDI";
    putc(0x00, midiOutFile); numBytes +=1; // delta 0
    putc(0xFF, midiOutFile); numBytes +=1; // meta event
    putc(0x04, midiOutFile); numBytes +=1; // type: instrument name
    numBytes += writeVarLen(strlen(instrumentName), midiOutFile); // length of name
    fwrite(instrumentName, sizeof(char), strlen(instrumentName), midiOutFile);
                                            // name itself
    numBytes += strlen(instrumentName);

    // for some reason this adds ~three seconds onto the beginning of the file
    // commenting out for now
    //numBytes += writeMidiText(0x01, "Create by midiRecord (http://zeitkunst.org/projects/midiRecord)", midiOutFile);

    // set this up to be a piano
    putc(0x00, midiOutFile); numBytes +=1; // delta 0
    putc(0xC0, midiOutFile); numBytes +=1; // program change
    putc(0x00, midiOutFile); numBytes +=1; // instrument 0 (grand piano)

    // RtMidiIn constructor
    try {
        midiin = new RtMidiIn();
    }
    catch (RtError &error) {
        error.printMessage();
        exit(EXIT_FAILURE);
    }

    // Check available ports vs. specified.
    unsigned int nPorts = midiin->getPortCount();
    if ( port >= nPorts ) {
        delete midiin;
        std::cout << "Invalid port specifier!\n";
        usage();
    }

	for (int i=0; i<nPorts; i++) {
		//std::cout << "Port " << i << ": " << midiin->getPortName(i) << std::endl;
	}

    try {
        midiin->openPort( port );
    }
    catch (RtError &error) {
        error.printMessage();
        goto cleanup;
    }

    // ignore sysex, don't ignore timing or active sensing messages.
    midiin->ignoreTypes( true, false, false );

    // Install an interrupt handler function.
    done = false;
    (void) signal(SIGINT, finish);

    // Periodically check input queue.
    //std::cout << "Reading MIDI from port ... quit with Ctrl-C.\n";

    while ( (!done) || (nBytes != 0) ) {
        stamp = midiin->getMessage( &message );
        runningStamp += stamp;
        nBytes = message.size();

        if ( nBytes > 0 ) {
            if (verbose) {std::cout << "stamp = " << stamp << " (running " << runningStamp << "): ";}
            deltaTime = (unsigned long) (1000 * stamp);
            //if (verbose) {std::cout << "stamp (ms) = " << deltaTime << "\n";}
            numBytes += writeVarLen(deltaTime, midiOutFile);
            if (message[0] == 248) {
                std::cout << runningStamp << ",TimingClock,'248'" << std::endl;
            }
            if (message[0] == 153) {
                std::cout << runningStamp << ",NoteOn Channel: 9,'153'" << std::endl;
            }
        }
        for ( i=0; i<nBytes; i++ ) {
            numBytes += 1;
            if (verbose) {std::cout << "Byte " << i << " = " << (int)message[i] << ", ";}
            putc((int)message[i], midiOutFile);
        }
        if (verbose && nBytes > 0) {
            std::cout << '\n';
        }

        if (quitTimeSeconds > 0 && runningStamp > quitTimeSeconds) {
            break;
        }

        // Sleep for 10 milliseconds.
        SLEEP( 10 );
    }

    // Clean up
 cleanup:
    midiin->closePort();
    delete midiin;

    // Through in a dummy note on and note off event to finish things up.
    // For some reason, without this extra time, I lose the last note that
    // I've played (at least when listening in quicktime), even though
    // the note on/note off pair show up in the midi stream.  Strange.
    // Total hack.  Please let me know how to fix it.
    numBytes += writeVarLen(deltaTime, midiOutFile);
    fputc(0x90, midiOutFile); numBytes +=1;
    fputc(0x54, midiOutFile); numBytes +=1;
    fputc(0x00, midiOutFile); numBytes +=1;

    numBytes += writeVarLen(deltaTime + 900, midiOutFile);
    fputc(0x80, midiOutFile); numBytes +=1;
    fputc(0x54, midiOutFile); numBytes +=1;
    fputc(0x00, midiOutFile); numBytes +=1;

    // write the end of track marker
    fputc(0xFF, midiOutFile); // meta event
    fputc(0x2F, midiOutFile); // end of track
    fputc(0x00, midiOutFile); // length 0

    // write the total number of events
    fseek(midiOutFile, 18, SEEK_SET);
    if (verbose) {std::cout << "numBytes = " << numBytes << "\n";}
    fwrite(&numBytes, sizeof(unsigned long), 1, midiOutFile);
    fclose(midiOutFile);

    return 0;
}

midiRecord -- record an incoming MIDI stream to disk
====================================================

Nick Knouf
<nknouf@zeitkunst.org>

Version 0.1

From my observations, there didn't seem to be any easy way to save an incoming MIDI stream to disk on OS X: at least, without having to purchase expensive tools such as Logic, Digital Performer, or Garage Band.  I came to this need one evening when I was pounding away at my MIDI-fied keyboard and decided that I wanted to save my sessions for later perusal and perhaps transcription.  I thought that this would be a common desire amongst people, but given the lack of tools to do so, perhaps not.

In any case, midiRecord allows you to save the incoming MIDI stream to a MIDI file, available for playing in any software that can handle MIDI files (and that also implement SMPTE timing, unlike Garage Band).

midiRecord depends on the RtMidi library by Gary Scavone (http://www.music.mcgill.ca/~gary/rtmidi/) for its MIDI handling.

I have tested this on OS X.4.7, but it should work anywhere else that RtMidi works, I believe.

How to Use
==========

usage: midiRecord <args>
    --file, -f      output filename (default: output.mid)
    --help, -h      this help information
    --port, -p      MIDI port number to use (default: 0)
    --verbose, -v   Display information about bytes being received

Building
========

Simply type 'make'.

Notes
=====

The following are hacks and issues:

*  I use SMPTE timing to get accurate, millisecond level timing of the performance.  This prevents its use in GarageBand, but can be changed via a simple comment in the code.  I might make this a command-line option in the future.
*  There is a hack where I had a dummy note at the end of a performance; without it, the final note is cut off.  Let me know how to fix this, if possible.
*  I haven't tried this for more than note on and note off, but I don't see anything in the code that should prevent it from handling other types of MIDI data.
